import logger from '../controllers/log_controller.js'

/**
 * 
 * @param {import("express").Request} req 
 * @param {import("express").Response} res 
 * @param {import("express").NextFunction} next 
 */
const entryM = (req, res,next) => {
    const endpoint = req.baseUrl +req.path;
    logger.info(`Entry: ${endpoint}`)
    next();
}

export default entryM;