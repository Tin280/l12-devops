import logger from '../controllers/log_controller.js'

/**
 * 
 * @param {import ("express").ErrorRequestHandler} err 
 * @param {import ("express").Request} req 
 * @param {import ("express").Response} res 
 * @param {import ("express").NextFunction} next  
 */
const json_sercurity = (err, req, res,next) => {
    if(err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        logger.notice(`Invalid JSON from: ${req.ip}`);
        return res.sendStatus(400);
    }
    next();
};

export default json_sercurity;