import app from './service.js'
import CONF from './config.js'
console.log("REST API service starting")

app.listen(3000, "127.0.0.1", () => {
    console.log("Listening: " + CONF.BASE_URL);
})