import express from 'express'
import apiV1 from './routes/v1/router.js'
import entryM from './middlewares/entry.middleware.js';
import routes from './routes/v1/scoreboard.js';
const app = express();

app.use(entryM);
app.get('/', async(req,res) => {
    await res.send("Welcome to the Scoreboard REST API");
})

app.use('/api/v1',apiV1)
app.use('/api/v1',routes)
export default app;
//export to main.js